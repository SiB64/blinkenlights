//
// bate.c
//

// !!! int = int8_t

#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/wdt.h>

#include "eeprom.h"
#define eeprom_read_byte unsafe_eeprom_read_byte

#define Bit(x) (1<<(x))
#define SetPORT(x) PORTB |= Bit(x)
#define ClrPORT(x) PORTB &=~ Bit(x)
#define GetPORT(x) (PINB & Bit(x))

#define RES_PORT  0
#define TRIG_PORT 1
#define OUT_PORT  2
#define LED2_PORT 3
#define LED1_PORT 4

#define MCLK_PORT RES_PORT
#define DOUT_PORT TRIG_PORT
#define SCK_PORT  LED2_PORT
#define DIN_PORT  LED1_PORT
#ifndef WDT_TRIGGERED
    // No free port for TxD.
    // Use LED1 for both DIN and TxD.
#   define TxD_PORT  DIN_PORT
#else
    // Remember to remove the OUT jumper!
    // This port becomes an output.
#   define TxD_PORT  OUT_PORT
#endif

#define PORTB_IO _SFR_IO_ADDR(PORTB)
#define PINB_IO  _SFR_IO_ADDR(PINB)

__attribute__((always_inline))
static inline
void r2port(int p, uint8_t c, int b)
{
#if 0
	if (c & (1<<b))
		SetPORT(p);
	else
		ClrPORT(p);
#else
	__asm__("sbrc %[c], %[b]"   "\n\t"
		"sbi %[PB], %[p]"   "\n\t"
		"sbrs %[c], %[b]"  "\n\t"
		"cbi %[PB], %[p]"   "\n\t"
		:
		: [p] "n" (p),
		  [c] "r" (c),
		  [b] "n" (b),
		  [PB] "n" (PORTB_IO)
		: "memory"
		);
#endif
}


////////////////////////////////////////////////////////////////////////////////
//
//  EEPROM config

struct bate_conf {
	uint8_t serial_number;
	uint8_t period;
	uint8_t wdt_period;
};

#define EE_ADDR(c) &(((struct bate_conf*)0)->c)
#define EE_CONF(c) eeprom_read_byte(EE_ADDR(c))

////////////////////////////////////////////////////////////////////////////////
//
//  Timer1 generates a 32768 Hz clock on OC0A, PB0, MCLK_PORT, RES_PORT


// The nominal f_{clkio} is 9.6 MHz,
// f_{MCLK} needs to be 32768 Hz
// f_{tick} needs to be 2×f_{MCLK}
// Timer 1 TOP needs to be f_{clkio}/65536

#ifndef PERIOD
#    define PERIOD 146
#endif

// Save the calibrated period in EEPROM

static inline
void init_timer()
{
	uint8_t p = EE_CONF(period);
	if (p < 120 || p > 200)
		p = PERIOD;

	TCCR0A = 0x43; // PWM TOP=OCRA TOV=OCRA
	TCCR0B = 0x09; // clk_IO/1
	OCR0A  = p;
	TIMSK0 = Bit(OCIE0A);
}

volatile uint8_t tick;

#if 0
ISR(TIM0_COMPA_vect)
{
	tick = 1;
}
#else
ISR(TIM0_COMPA_vect, ISR_NAKED)
{
	__asm__ ("push r24"         "\n\t"
		 "ldi r24,1"        "\n\t"
		 "sts %[tick], r24" "\n\t"
		 "pop r24"          "\n\t"
		 "reti"             "\n"
		 :[tick] "+m" (tick)
		);
}
#endif

////////////////////////////////////////////////////////////////////////////////
//
//  Trigger by the NE555

volatile uint8_t trigger;

#ifndef WDT_TRIGGERED

static inline
void init_trig()
{
	PCMSK = Bit(OUT_PORT);
	GIMSK = Bit(PCIE);
}

#if 0
ISR(PCINT0_vect)
{
	if (GetPORT(OUT_PORT))
		trigger = 1;
}
#else
ISR(PCINT0_vect, ISR_NAKED)
{
	__asm__("push r24"           "\n\t"
		"ldi r24, 1"         "\n\t"
		"sbic %[PB], %[b]"   "\n\t"
		"sts %[trigger], r24" "\n\t"
		"pop r24"            "\n\t"
		"reti"               "\n\t"
		:[trigger] "+m" (trigger)
		:[PB] "n" (PINB_IO),
		 [b] "n" (OUT_PORT)
		);
}
#endif

#endif // !WDT_TRIGGERED

////////////////////////////////////////////////////////////////////////////////
//
//  Trigger by the WatchDogTimer

#ifdef WDT_TRIGGERED

uint8_t EEMEM wdt_period;

void init_wdt()
{
	uint8_t p = EE_CONF(wdt_period);
	if (p>0x0f)
		p = 6; // 1s 
	uint8_t cr = Bit(WDE) | (p&7) << WDP0 | (p&8) << (WDP3-3);
	__asm__("STS %[CSR], %[CE]"     "\n\t"
		"STS %[CSR], %[WE]"     "\n"
		::[CSR] "n" (&WDTCR),
		 [CE] "r" ((Bit(WDCE) | Bit(WDE))),
		 [WE] "r" (cr)
		);
}

#endif // WDT_TRIGGERED

/////////////////////////////////////////////////////////////////////////////
//
// Send the pressure data via 2400 baud UART

#define BAUD 2400

static uint16_t ucount;

static inline
uint8_t uart_tick()
{
	// The tick frequency is 2¹⁶ Hz,
	// to generate 32768 Hz MCLK.
	ucount += BAUD;
	return ucount < BAUD;
}

static uint8_t send_wait;
static uint8_t send_bytes;
static uint8_t send_char;
static uint8_t send_bits;

#define NSTOP 3

static inline
void send(uint8_t c)
{
	send_char = c;
	send_bits = 9+NSTOP;
}

static inline
void send_bit() {
	if (!send_bits)
		return;
	send_bits--;
	if (send_bits >= 8+NSTOP)
		ClrPORT(TxD_PORT);
	else if (send_bits < NSTOP)
		SetPORT(TxD_PORT);
	else {
		r2port(TxD_PORT, send_char, 0);
		send_char >>= 1;
	}
}

union {
	uint8_t b[16];
	uint16_t w[8];
	struct {
		uint16_t H[2];	
		uint16_t W[4];
		uint16_t D[2];
	};
	struct {
		uint16_t H1;
		uint16_t H2;
		uint16_t W1;
		uint16_t W2;
		uint16_t W3;
		uint16_t W4;
		uint16_t D1;
		uint16_t D2;
	};
} bate;

static inline
void uart()
{
	if (!uart_tick())
		return;
	send_bit();
	if (send_bits)
		return;
	if (send_wait) {
		SetPORT(TxD_PORT);
		send_wait--;
		return;
	}
	if (send_bytes)
		send(bate.b[sizeof(bate)-send_bytes--]);
#   ifndef WDT_TRIGGERED
	else
		// Send break between conversions,
		// to turn off the LED
		ClrPORT(TxD_PORT);
#   endif
}

static inline
void send_bate()
{
	send_wait = 20;
	send_bytes = sizeof(bate);
}

static inline
uint8_t uart_busy()
{
	return send_bytes || send_bits;
}

/////////////////////////////////////////////////////////////////////////////
//
// Read data from an MS5534C pressure sensor

__attribute__ ((noinline, noclone))
static
uint8_t bate_bit(uint8_t r, uint8_t c, uint8_t ii)
{
#if 0
	uint8_t clkl = 10;
	uint8_t clkh = 30;
	if (c & ii)
		PORTB |= Bit(DIN_PORT);
	else
		PORTB &=~ Bit(DIN_PORT);
	if (PINB & Bit(DOUT_PORT))
		r |= ii;
	else
		r &= ~ii;
	while(clkl)
		clkl--;
	PORTB |= Bit(SCK_PORT);
	while(clkh)
		clkh--;
	PORTB &=~ Bit(SCK_PORT);
#else
	__asm__(
		"and %[c], %[ii]"         "\n\t"
		"brne .+2"                "\n\t"
		"cbi %[PORT], %[DIN]"     "\n\t"
		"breq .+2"                "\n\t"
		"sbi %[PORT], %[DIN]"     "\n\t"
		"sbic %[PIN], %[DOUT]"    "\n\t"
		"or %[r], %[ii]"          "\n\t"
		"com %[ii]"               "\n\t"
		"sbis %[PIN], %[DOUT]"    "\n\t"
		"and  %[r], %[ii]"        "\n\t"

		"nop"                     "\n\t"
		"nop"                     "\n\t"
		"nop"                     "\n\t"
		"nop"                     "\n\t"
		"nop"                     "\n\t"

		"sbi %[PORT], %[SCK]"     "\n\t"

		"nop"                     "\n\t"
		"nop"                     "\n\t"
		"nop"                     "\n\t"
		"nop"                     "\n\t"
		"nop"                     "\n\t"
		"nop"                     "\n\t"
		"nop"                     "\n\t"
		"nop"                     "\n\t"
		"nop"                     "\n\t"
		"nop"                     "\n\t"

		"cbi %[PORT], %[SCK]"     "\n"
		
		:[r]   "+r" (r),
		 [c]   "+r" (c)
		:[ii]  "r" (ii),
		 [PORT] "n" (PORTB_IO),
		 [PIN]  "n" (PINB_IO),
		 [DIN]  "n" (DIN_PORT),
		 [DOUT] "n" (DOUT_PORT),
		 [SCK]  "n" (SCK_PORT)
		: "memory"
		);
#endif
	return r;
}

__attribute__ ((noinline, noclone))
static
uint16_t bate_frame(uint16_t d, uint8_t n)
{
	n--;
	uint8_t c0 = d;
	uint8_t c1 = d >> 8;
	uint8_t r0;
	uint8_t r1;
	// Force r0 and r1 to be cleared before the switch.  Else, the
	// compiler generates extra jumps to clear r0 for each target.
	__asm__("clr %[r0]"   "\n\t"
		"clr %[r1]"   "\n"
		:[r0] "=r" (r0),
		 [r1] "=r" (r1)
		);
	switch (n) {
	case 15: r1 = bate_bit(r1, c1, 1<<7);
	case 14: r1 = bate_bit(r1, c1, 1<<6);
	case 13: r1 = bate_bit(r1, c1, 1<<5);
	case 12: r1 = bate_bit(r1, c1, 1<<4);
	case 11: r1 = bate_bit(r1, c1, 1<<3);
	case 10: r1 = bate_bit(r1, c1, 1<<2);
	case  9: r1 = bate_bit(r1, c1, 1<<1);
	case  8: r1 = bate_bit(r1, c1, 1<<0);
	case  7: r0 = bate_bit(r0, c0, 1<<7);
	case  6: r0 = bate_bit(r0, c0, 1<<6);
	case  5: r0 = bate_bit(r0, c0, 1<<5);
	case  4: r0 = bate_bit(r0, c0, 1<<4);
	case  3: r0 = bate_bit(r0, c0, 1<<3);
	case  2: r0 = bate_bit(r0, c0, 1<<2);
	case  1: r0 = bate_bit(r0, c0, 1<<1);
	case  0: r0 = bate_bit(r0, c0, 1<<0);
	}
	return (uint16_t)r1<<8 | r0;
}

__attribute__ ((noinline, noclone))
static
void bate_wait()
{
	uint16_t timeout = 3277; // 50ms
	sei();
	while (PINB & Bit(DOUT_PORT)) {
		if (tick) {
			tick = 0;
			if (!timeout)
				break;
			wdt_reset();
			timeout--;
		}
	}
	cli();
}

static inline
void read_bate()
{
	cli();
	bate.H1 = 0x0001;
	bate_frame(0xaaaa, 16);
	bate_frame(0, 5);
	bate_frame(0x3aa0, 14);
	bate.W1 = bate_frame(0, 16);
	bate_frame(0x3ac0, 14);
	bate.W2 = bate_frame(0, 16);
	bate_frame(0x3b20, 14);
	bate.W3 = bate_frame(0, 16);
	bate_frame(0x3b40, 14);
	bate.W4 = bate_frame(0, 16);
	bate_frame(0x0f40, 12);
	bate.H1 = 0x0002;
	bate_wait();
	bate_frame(0,1);
	bate.D1 = bate_frame(0, 16);
	bate_frame(0x0f20, 12);
	bate.H1 = 0x0003;
	bate_wait();
	bate_frame(0,1);
	bate.D2 = bate_frame(0, 16);
	bate.H1 = 0xba7e;
#   ifndef WDT_TRIGGERED
	bate.H2 += 1;
#   endif
}

int main()
{
	PRR = 1<<PRADC;
	DDRB = Bit(MCLK_PORT) | Bit(SCK_PORT) | Bit(DIN_PORT) | Bit(TxD_PORT);

	init_timer();

#   ifndef WDT_TRIGGERED
	init_trig();
#   else
	init_wdt();
	trigger = 1;
	SetPORT(TxD_PORT);
	bate.b[2] = EE_CONF(serial_number);
	bate.b[3] = OCR0A;
#   endif

	// send some MCLKs before we start
	uint8_t mclk_delay = 0xff;

	while (1) {
		sei();
		MCUCR = Bit(SE) | SLEEP_MODE_IDLE;
		sleep_cpu();
		if (!tick)
			continue;
		tick = 0;
		wdt_reset();
		uart();
		if (uart_busy())
			continue;
		if (mclk_delay) {
			mclk_delay--;
			continue;
		}
		if (!trigger) {
#           ifndef WDT_TRIGGERED
			// wait for PCINT on PORT_OUT
			continue;
#           else
			// power down, wait for the WDT
			MCUCR = Bit(SE) | SLEEP_MODE_PWR_DOWN;
			cli();
			sleep_cpu();
#           endif
		}
		trigger = 0;
		// do a conversion and submit the result to uart()
		read_bate();
		send_bate();
	}
}
