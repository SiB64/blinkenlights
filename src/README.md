# Blinkenlights Code

Here you will find

1. Code to flash into the ATtiny13a, written in C (avr-gcc, avr-libc).

    1. `blink.c`: A simple _Hello World_ type of demo.

       LED1 with turn on and exponetionally decay in brightness after a
       keypress or trigger by the NE555.

    2. `kennung.c`: LED1 will emit a characteristic light pattern.

       The NE555 provides the base period.  The ATtiny loads a pattern
       from EEPROM and emits the pattern, on bit for each tick on `out`.
       Via a UART (TTL level) a new pattern can be loaded to RAM or EEPROM.

    3. `bate.c`: Readout of an MS5534C Pressure Sensor.
    
       Generate SPI frames for the sensor, capture the returned bits and send
       them binary via UART.

2. Code to talk to the ATtiny via a UART, written in Python.

    1. `karte1.py`: Parses strings found on sea maps next to sea marks
        with lights and provides the characteritics light patterns

    2. `kennung.py`: Reads or loads those patterns via UART into the
        ATtiny.

    3. `bate.py`: Capture the pressure data, apply calibration and print
       clear text.

3. `mux_uart.c`: A small utility to configure the gpio pins on a
   Raspberry Pi, written in C.

   `avr-dude` can use any pins on a Raspberry Pi to flash an AVR µC in
   `linuxgpio` mode.  The `TxD` (gpio14) and `RxD` (gpio15) pins can
   be connected as `MOSI` and `MISO` for flashing and later be used for
   UART communication without the need to change the connections.
   After `avr-dude` used the pins for flashing, they are not
   multiplexed to the UART any more.  This tool restores that
   connection without the need to reboot the Raspberry Pi.

   `mux_uart` can be used to change the FSEL bits of any pin.  Don't!
   Unless you know what you are doing.

![RPi2-Blinkenlights](https://codeberg.org/SiB64/blinkenlights/raw/branch/master/src/avrdude-rpi.png)
