
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/mman.h>

/*******************************************************************

# mux_uart.c (c) Stephan Böttcher

Connect gpio 14, 15 of the Raspberry Pi to TxD, RxD of the UART
/dev/ttyAMA0

usage:
- `mux_uart` show the current connections
- `mux_uart set`  connect to the UART

The gpio subsystem disconnects the pins from the UART when they are
exported.  When unexported the connection is not restored.  

The usecase is a connection of an AVR µC to those pins for alternating
use to flash the program with avrdude in linuxgpio mode and subsequent
communication via the UART.  This tools avoid a reboot of the
raspberry pi after flashing the µC.

The comments below may contain inaccuracies resultung of some
misunderstandings on my part.  I did not read the datasheet of the
Raspberry Pi's processor.

The Linux kernel exports the IO memory space via `mmap()` of
`/dev/gpiomem`.  That file is typically accessible by group `dialout`,
a previlige that is needed to use the UART as well.

Located at `GPIO_ADDR` are registers controlling the multiplexing of
alternate functions to IO pins and other pinctrl settings.  The first
six addresses are the `GPFSEL` registers that assign one of eight
functions to each IO pin, 10 pins per register, three bit per pin.
The index is the `gpio` number, as in `/sys/class/gpio/gpio*`.

The `FSEL` codes are named

0: gpio input
1: gpio output
2: alternate function 5
3: alternate function 4
4: alternate function 0
5: alternate function 1
6: alternate function 2
7: alternate function 3

Pins 14 and 15 connect to the UART as alternate function 0, mux=4.

See: BCM2835-ARM-Peripherals.pdf

*******************************************************************/

#define GPIO_ADDR 0x00200000
static volatile uint32_t *gpio;

static struct pin_t {
	char *name;
	unsigned int gpio, mux;
} pins[8] =
{
	[0] = { .name="TxD", .gpio=14, .mux=4 },
	[1] = { .name="RxD", .gpio=15, .mux=4 },
	[2] = {},
};

static inline
unsigned int get_mux(struct pin_t *p)
{
	unsigned int s = 3*(p->gpio % 10);
	unsigned int a = p->gpio / 10;
	return (gpio[a] >> s) & 7;
}

static inline
void set_mux(struct pin_t *p)
{
	unsigned int s = 3*(p->gpio % 10);
	unsigned int a = p->gpio/10;
	uint32_t v = gpio[a];
	v &= ~(7 << s);
	v |= (p->mux & 7) << s;
	gpio[a] = v;
}

int main(int argc, char **argv)
{
	int i = 1;
	int set = 0;
	while (i<argc) {
		if (!strcmp(argv[i], "rpi2")) {
			pins[0].mux=4;
			pins[1].mux=4;
		}
		else if (!strcmp(argv[i], "rpi3")) {
			pins[0].mux=2;
			pins[1].mux=2;
		}
		else if (!strcmp(argv[i], "set"))
			set = 1;
		else
			break;
		i++;
	}

#ifdef WITH_ARGS
	void usage(const char *m)
	{
		fprintf(stderr, "usage: \n"
			" mux_uart                      read UART FSEL bits\n"
			" mux_uart set                  set UART FSEL bits to ALT0\n"
			" mux_uart gpio «n» …           read FSEL bits\n"
			" mux_uart mux «m» gpio «n» …   set FSEL bits (up to 7 pins)\n"
			"\n"
			"? %s\n", m);
		exit(2);
	}

	int n = 0;
	int mux = -1;
	while (i < argc) {
		char *e;
		if (!strcmp(argv[i], "gpio")) {
			i++;
			int j = 0;
			while (i<argc && n < sizeof(pins)/sizeof(*pins) - 1) {
				long g = strtol(argv[i], &e, 0);
				if (*e) {
					if (j)
						break;
					usage(argv[i]);
				}
				j++;
				pins[n].name = argv[i];
				pins[n].gpio = g & 63;
				if (mux >= 0)
					pins[n].mux = mux & 7;
				n++;
				pins[n].name = NULL;
				i++;
			}
			if (!j) 
				usage(argv[i-1]);	
		}
		else if (!strcmp(argv[i], "mux")) {
			if (i+1 >= argc)
				usage(argv[i]);
			long m = strtol(argv[i+1], &e, 0);
			if (*e)
				usage(argv[i+1]);
			if (mux < 0) 
				for (int j=0; j<n; j++)
					pins[j].mux = m & 7;
			mux = m;
			i += 2;
			set = 1;
		}
		else
			usage(argv[i]);
	}
#endif

	int fd;
	fd = open("/dev/gpiomem", O_RDWR | O_SYNC | O_CLOEXEC);
	if (fd<0) {
		perror("open /dev/gpiomem");
		exit(1);
	}
	
	gpio = mmap(0, 4096, PROT_READ|PROT_WRITE, MAP_SHARED, fd, GPIO_ADDR);
	if (gpio == MAP_FAILED) {
		perror("mmap /dev/gpiomem");
		exit(1);
	}
	
	for (struct pin_t *p=pins; p->name; p++) {
		int o = get_mux(p);
		if (set) {
			set_mux(p);
			printf("Pin[%d] %s: mode set from %d to %d results is %d\n",
			       p->gpio, p->name, o, p->mux, get_mux(p));
		}
		else
			printf("Pin[%d] %s: mode is %d\n",
			       p->gpio, p->name, o);
	}
	
	return 0;
}
