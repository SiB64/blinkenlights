
#include <stdint.h>
#include <avr/io.h>
#include <avr/eeprom.h>

// Entirely unsafe EEPROM access. Just wait 5ms between calls
// int = byte

static inline
void unsafe_eeprom_write_byte(uint8_t *addr, uint8_t data)
{
	EEARL = (uint8_t)(uint16_t)addr;
	EEDR = data;
	EECR |= (1<<EEMPE);
	EECR |= (1<<EEPE);
}

static inline
uint8_t unsafe_eeprom_read_byte(uint8_t *addr)
{
	EEARL = (uint8_t)(uint16_t)addr;
	EECR |= (1<<EERE);
	return EEDR;
}

static inline
void unsafe_eeprom_read_block(void *dest, void *src, uint8_t size)
{
	uint8_t *d = dest;
	uint8_t a = (uint8_t)(uint16_t)src;
	while (size--) {
		EEARL = a++;
		EECR |= (1<<EERE);
		*(d++) = EEDR;
	}
}
