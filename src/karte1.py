#! /usr/bin/ipython3 --profile=karte1

import sys, re

def set_prompt(prompt):
    sys.ps1 = prompt + "> "
    sys.ps2 = prompt + ". "
    try:
        ip=get_ipython()
        from IPython.terminal.prompts import Prompts, Token
        class myprompts(Prompts):
            def in_prompt_tokens(self, *wtf):
                return [(Token, prompt+'> ')]
            def continuation_prompt_tokens(self, *wtf):
                return [(Token, prompt+'. ')]
            def out_prompt_tokens(self, *wtf):
                return [(Token, prompt+'= ')]
            def rewrite_prompt_tokens(self, *wtf):
                return [(Token, prompt+'- ')]
        ip.prompts=myprompts(ip)
    except NameError:
        pass

set_prompt("K1")

class Tonnen_Kennung():

    re = re.compile("".join((
        "(?P<Muster>X|Oc|Iso|LFl|Fl|F|Q|VQ|UQ|Mo|IQ)",
        "(?: *\((?:",
        "(?:(?P<Anzahl>[0-9]+)(?:\+(?P<Mehr>[0-9]+))?)",
        "(?:, *(?P<Bits>((?:0[bx])?[0-9a-f]+)))?",
        "|(?P<Morse>[ A-ZÄÖÜ0-9@.-]+)",
        ")\))?",
        "(?:\+(?P<Extra>LFl|Fl))?",
        "(?:[ .]*(?P<Farbe>[GRWY]+))?",
        "(?:[ .]*(?P<Dauer>[0-9.]+)s)?",
        "(?:[ .]*(?P<Höhe>[0-9.]+)m)?",
        "(?:[ .]*(?P<Weite>[0-9.]+)M)?",
    )))

    def __init__(self, Kennung=None, Muster="F", Faktor=None, **aa):
        self.Kennung = None
        self.Anzahl  = 1
        self.Mehr    = None
        self.Morse   = ''
        self.Farbe   = ''
        self.Dauer   = 0
        self.Höhe    = 0
        self.Weite   = 0
        self.Extra   = None
        self.update(Muster=Muster, **aa)
        if Kennung is not None:
            self.parse(Kennung)
        if Faktor:
            self.Geschwindigkeit(Faktor)

    def parse(self, Kennung):
        self.update(**self.re.fullmatch(Kennung).groupdict())
        self.Kennung = Kennung

    def __str__(self):
        r = self.Muster
        if r == "X":
            r += "(%d,0x%x)" % (self.Anzahl, self.Bits)
        elif r == "Mo":
            r += "(%s)" % self.Morse
        elif self.Anzahl:
            r += "(%d%s)" % (self.Anzahl, "+%d" % self.Mehr if self.Mehr else "")
        if self.Extra:
            r += "+"+str(self.Extra)
        if self.Farbe:
            if r[-1] != ")":
                r += " "
            r += self.Farbe
        if self.Dauer:
            r += ".%.3gs" % self.Dauer
        if self.Höhe:
            r += " %.3gm" % self.Höhe
        if self.Weite:
            r += " %.3gM" % self.Weite
        return r

    def __repr__(self):
        return "Tonnen_Kennung('%s')" % str(self)

    def update(self, Muster=None,
               Anzahl=None, Mehr=None, Bits=None, Morse=None,
               Extra=None,
               Farbe=None, Dauer=None, Höhe=None, Weite=None, **aa):
        if Muster is not None:
            self.get_seq = self.__getattribute__(Muster)
            self.Muster = Muster
        if Anzahl is not None:
            self.Anzahl = int(Anzahl)
        if Mehr is not None:
            self.Mehr = int(Mehr)
        if Bits is not None:
            self.Bits = int(Bits, 0)
        if Morse is not None:
            self.Morse = Morse
        if Dauer is not None:
            self.Dauer = float(Dauer)
        if Farbe is not None:
            self.Farbe = Farbe
        if Höhe is not None:
            self.Höhe = float(Höhe)
        if Weite is not None:
            self.Weite = float(Weite)
        if Extra is not None:
            self.Extra = Tonnen_Kennung(Extra)
        if Muster == "Mo" and not self.Morse and Anzahl:
            self.Morse = Anzahl

    OnOff = dict(
        X = (0.5,),
        F = (),
        Iso = (2,),
        Fl = (1, 2),
        Oc = (2, 1),
        LFl = (2.5, 4.5),
        Q = (0.5,),
        VQ = (0.25,),
        UQ = (0.125,),
        IQ = (0.5,),
        Mo = (1, 3, 7),
    )

    def Geschwindigkeit(self, Faktor):
        self.OnOff = {n: tuple(v*Faktor
                               for v in vv)
                      for n, vv in Tonnen_Kennung.OnOff.items()}

    def sequence(self):
        seq = self.get_seq(*self.OnOff[self.Muster])
        if self.Extra is not None:
            eseq = self.Extra.sequence()
            d = seq[-1]
            seq[-1] = seq[1]
            seq.append(eseq[0])
            seq.append(d - seq[1] - seq[-1])
        return seq

    def X(self, t):
        if self.Dauer:
            t = self.Dauer/self.Anzahl
        kk = self.Bits
        if not kk:
            return []
        k = 0
        for i in range(self.Anzahl):
            k <<= 1
            k |= kk & 1
            kk >>= 1
        while k and not k&1:
            k >>= 1
        v = k&1
        n = 0
        seq = []
        for i in range(self.Anzahl):
            if v == (k&1):
                n += 1
            else:
                seq.append(n*t)
                v = k&1
                n=1
            k >>= 1
        seq.append(n*t)
        return seq

    def F(self):
        self.seq = [1.0]

    def Fl(self, an=1, aus=2):
        seq = [an,aus] * self.Anzahl

        if self.Mehr:
            seq2 = [an,aus] * self.Mehr
            if not self.Dauer:
                seq[-1] = 2*aus
                seq2[-1] = 2*aus
                return seq+seq2
            d = self.Dauer - sum(seq+seq2) 
            if d > 1.9*an:
                seq[-1] += d/2
                seq2[-1] += d/2
                return seq+seq2
            seq[-1] += aus
            seq.extend(seq2)
            Pause = aus
        else:
            if not self.Dauer:
                if self.Anzahl > 1:
                    seq[-1] +=  aus
                return seq
            Pause = 0
            if self.Anzahl > 1:
                Pause = aus
                
        d = sum(seq)
        seq[-1] += (self.Dauer - d)
        if self.Dauer >= d + 0.9*Pause:
            return seq
        d += Pause
        an *= self.Dauer/d
        aus *= self.Dauer/d
        return self.Fl(an, aus)

    def Oc(self, an=2, aus=1):
        seq = self.Fl(aus, an)
        seq.reverse()
        return seq

    def LFl(self, an=2.5, aus=4):
        return self.Fl(an, aus)

    def Q(self, an=0.5):
        return self.Fl(an,an)

    def VQ(self, an=0.25):
        return self.Q(an)

    def UQ(self, an=0.125):
        return self.Q(an)

    def IQ(self, aus=0.5):
        seq = self.Q(aus)
        seq.reverse()
        return seq

    def Iso(self, an=2):
        if self.Dauer is not None:
            an = self.Dauer/2
        return (an, an)

    def Mo(self, Punkt=1, Strich=3, Wort=7):
        seq = []

        for c in self.Morse:
            if c==' ':
                if seq:
                    seq.append(Wort)
                continue
            if seq:
                seq.append(Strich)
            ss = {'·': Punkt, '-': Strich}
            s=tuple(sum((map(lambda cc: [ss[cc],Punkt], self.Morsealphabet[c])),start=[]))
            seq.extend(s[:-1])
        if len(self.Morse)==1:
            Pause = Strich
        else:
            Pause = Wort
        if not self.Dauer:
            seq.append(Pause)
        else:
            d = sum(seq)
            if d + 0.9*Pause <= self.Dauer:
                seq.append(self.Dauer - d)
            else:
                Punkt *= self.Dauer/(d + Pause)
                return self.Mo(Punkt, 3*Punkt, 5*Punkt)
        return seq

    Morsealphabet = {
        'A': '·-',
        'B': '-···',
        'C': '-·-·',
        'D': '-··',
        'E': '·',
        'F': '··-·',
        'G': '--·',
        'H': '····',
        'I': '··',
        'J': '·---',
        'K': '-·-',
        'L': '·-··',
        'M': '--',
        'N': '-·',
        'O': '---',
        'P': '·--·',
        'Q': '--·-',
        'R': '·-·',
        'S': '···',
        'T': '-',
        'U': '··-',
        'V': '···-',
        'W': '·--',
        'X': '-··-',
        'Y': '-·--',
        'Z': '--··',
        '1': '·----',
        '2': '··---',
        '3': '···--',
        '4': '····-',
        '5': '·····',
        '6': '-····',
        '7': '--···',
        '8': '---··',
        '9': '----·',
        '@': '·--·-·',
        '.': '·-·-·-',
        ',': '--··--',
        '?': '··--··',
        "'": '·----·',
        '!': '-·-·--',
        '/': '-··-·',
        '(': '-·--·',
        ')': '-·--·-',
        '&': '·-···',
        ':': '---···',
        ';': '-·-·-·',
        '=': '-···-',
        '+': '·-·-·',
        '-': '-····-',
        '_': '··--·-',
        '"': '·-··-·',
        '$': '···-··-',
        'Ä': '·-·-',
        'Æ': '·-·-',
        'Å': '·--·-',
        'À': '·--·-',
        'É': '··-··',
        'È': '·-··-',
        'Ñ': '--·--',
        'Ö': '---·',
        'Ø': '---·',
        'Ó': '---·',
        'Ü': '··--',
    }

    def get_Dauer(self):
        return sum(self.sequence())

    def Verlauf(self, an='💡', aus='🌊', *, Skala=True, Tick=None, Abweichung=0.25):
        A = int(1/Abweichung+0.9)
        if Skala is True:
            if len(an.encode())>2:
                Skala = 2
        seq=self.sequence()
        Dauer = sum(seq)

        mTick = Dauer
        for t in seq:
            if t < mTick:
                mTick = t

        mmTick = mTick
        if Tick:
            if Tick < mmTick:
                mmTick /= int(mmTick/Tick + Abweichung/2)
        else:
            aaa = 1
            nnn = 0
            for n in range(1,1+int(1/Abweichung+0.9)):
                aa = 0
                for t in seq:
                    nn = n*t/mmTick
                    aa = max(aa, abs(((nn+0.5) % 1 - 0.5))/nn)
                if aa < aaa:
                    aaa = aa
                    nnn = n
                if aa < Abweichung:
                    break
            if nnn:
                mmTick /= nnn

        if Tick and Tick > 1.25*mTick:
            raise ValueError("Taktperiode zu groß: %.gs > %.gs(%gs)"
                             % (Tick, mTick, mmTick))

        Tick = mmTick

        Ticks = [int(t/Tick+0.5) for t in seq]
        V = "".join([(an,aus)[i&1]*n for i,n in enumerate(Ticks)])
        if not Skala:
            return V
        T = "%.0fs" % (Tick*len(V))
        l = len(V)*Skala
        if len(T)+2 < l:
            ll = l - len(T) - 2
            M = "←" + "-"*(ll//2) + T + "-"*(ll-ll//2) + "→"
        else:
            ll = max(0, len(T)-2)
            M = "←" + "-"*ll + "→ " + T
        return V + "\n" + M + "\n(%gs/%gs)" % (mTick, mmTick)
