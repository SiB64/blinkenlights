#!/usr/bin/ipython3 --profile=kennung

import sys, os, serial, threading, struct, time
from karte1 import Tonnen_Kennung

def set_prompt(prompt):
    sys.ps1 = prompt + "> "
    sys.ps2 = prompt + ". "
    try:
        ip=get_ipython()
        from IPython.terminal.prompts import Prompts, Token
        class myprompts(Prompts):
            def in_prompt_tokens(self, *wtf):
                return [(Token, prompt+'> ')]
            def continuation_prompt_tokens(self, *wtf):
                return [(Token, prompt+'. ')]
            def out_prompt_tokens(self, *wtf):
                return [(Token, prompt+'= ')]
            def rewrite_prompt_tokens(self, *wtf):
                return [(Token, prompt+'- ')]
        ip.prompts=myprompts(ip)
    except ImportError:
        ip.prompt_manager.templates = {
            'in':  'In [{color.number}{count}{color.prompt}] '+prompt+'> ',
            'in2':                              '   .{dots}. '+prompt+'. ',
            'out': 'Out[{color.number}{count}{color.prompt}] '+prompt+'> ',
        }
    except NameError:
        pass

set_prompt("Kennung")

class Kennung(threading.Thread):

    verbose = False

    def __init__(self, port):
        self.readbuffer = b""
        self.reader_lock = threading.Lock()
        self.resp_ready = threading.Condition()
        self.responses = b""
        self.alive = False
        if port:
            self.connect(port)
        else:
            self.tty = None

    def connect(self, port, baudrate=2400):
        if self.alive:
            return
        self.serial = serial.Serial(baudrate=baudrate, parity='N', timeout=10.0,
                                    xonxoff=False, rtscts=False, dsrdtr=False)
        self.serial.port = port
        self.tty = port[5:]
        self.serial.open()
        self.alive = True
        threading.Thread.__init__(self, target=self.reader)
        self.daemon = True
        self.start()

    def _log(self, s):
        if isinstance(s, bytes):
            s = reps(s)
        sys.stderr.write(time.strftime("%Y-%m-%dT%H:%M:%SZ ", time.gmtime()) + s + "\n")

    def reader(self):
        data = b""
        while self.alive:
            data += self.serial.read(1)
            if data and self.reader_lock.acquire(False):
                self.readbuffer += data
                data = b""
                self.parser()
                self.reader_lock.release()

    def kill(self):
        self.alive = False
        self.join()

    sync = True
    show = False

    def parser(self):
        "do something with received chars in .readbuffer"
        if not self.resp_ready.acquire(False):
            return

        b = self.readbuffer
        self.readbuffer = b""
        if self.show:
            self._log("%s< %s" % (self.tty, repr(b)))

        self.responses += b

        if self.responses:
            self.resp_ready.notify()
        self.resp_ready.release()

    def resp(self, timeout=1, blocking=True):
        if not self.alive or not self.resp_ready.acquire(blocking):
            return b''
        if blocking and not self.responses:
            self.resp_ready.wait(timeout)
        l = self.responses
        self.responses = b""
        self.resp_ready.release()
        if self.verbose:
            self._log("%s< %s" % (self.tty, repr(l)))
        return l

    def cmd(self, c, a=None, b=None, *, repeat=True, **kk):
        if not self.alive:
            return b''
        bb = b''
        aa = b''
        if b is not None:
            bb = b'%d,' % b if b else b','
        if a:
            aa = b'%d' % a
        c = bb+aa+c
        if self.verbose:
            self._log("%s> %s" % (self.tty, repr(c)))
        while True:
            self.serial.write(c)
            r = b''
            for i in range(5):
                r += self.resp(**kk)
                if r[-1:]==c[-1:] or r[-1:].isdigit():
                    return r
                if r[-1:]==b'?':
                    break
            if not repeat or r[-1:]==c[-1:] or r[-1:].isdigit():
                return r
            repeat -= 1
            if self.verbose:
                self._log("%s> %s RETRY" % (self.tty, repr(c)))

    def reset_avr(self):
        self.cmd(b',55x', repeat=False, blocking=False)

    class KennungsFehler(Exception):
        pass

    eeprom_retrys = 2

    def write_kennung(self, a=0, b=0, *, eeprom=False, update=True, verify=True):
        b &= 0xff
        c = b'w'
        if eeprom:
            c = b'e'
        repeat = self.eeprom_retrys+1
        while True:
            if not repeat:
                raise self.KennungsFehler("write_kennung: eeprom too many retrys")
            repeat -= 1
            if eeprom and update:
                bb=self.read_kennung(a, eeprom=eeprom)
                if verbose:
                    self._log("eeprom [%d] old 0x%02x new 0x%02x %s"
                              % (a, bb, b, "same" if b==bb else "UPDATE"))
                if b==bb:
                    return
            r =  self.cmd(c, 0xff & b, a, repeat = not eeprom)
            if eeprom and r[-1:] == b'?':
                update = True
                continue
            if r[-1:] != c:
                raise self.KennungsFehler("write_kennung: invalid response: %s -> %s" % (c,r))
            if not eeprom or not verify:
                return
            bb=self.read_kennung(a, eeprom=eeprom)
            if b==bb:
                return
            self._log("eeprom [%d] wrote 0x%02x verify 0x%02x FAILED" % (a, bb, b))
            update = False

    def read_kennung(self, a=0, *, eeprom=False):
        c = b'r'
        if eeprom:
            c = b'v'
        n = 0
        for r in [self.cmd(c, s, a) for s in (6,3,0)]:
            if not r[-1:].isdigit():
                raise self.KennungsFehler("read_kennung: invalid response: %s" % r)
            n = 8*n + (r[-1]&7);
        return n

    def set_kennung(self, k, n=None, *, filename=None, eeprom=False):
        if n is None:
            nn = 0
            n = 0
            kk = k
            while kk >= 0x100:
                nn += 1
                kk >>= 8
            while kk:
                n += 1
                kk >>= 1
            n += 8*nn
            if not n:
                n += 1
        else:
            nn = n//8

        kk = 0
        kkk = k
        for i in range(n):
            kk <<= 1
            kk |= kkk & 1
            kkk >>= 1

        if filename:
            with sys.stdout.buffer if filename=='-' else open(filename, "wb") as f:
                f.write(bytes((n,)))
                for i in range(nn+1):
                    f.write(bytes(((kk>>(8*i)) & 0xff,)))
            return

        for i in range(nn+1):
            self.write_kennung(i+1, (kk>>(8*i))&0xff, eeprom=eeprom)
        self.write_kennung(0, n, eeprom=eeprom)

    def get_kennung(self, *, eeprom=False):
        n = 0
        k = 0

        n = self.read_kennung(0, eeprom=eeprom)
        for i in range(n//8+1):
            k |= self.read_kennung(i+1, eeprom=eeprom) << (8*i)

        kk = 0
        kkk = k
        for i in range(n):
            kk <<= 1
            kk |= kkk & 1
            kkk >>= 1

        return (kk, n)

    def Tonne(self, Kennung, *, Tick=0.5, filename=None, eeprom=False, **kk):
        if isinstance(Kennung, str):
            Kennung = Tonnen_Kennung(Kennung, **kk)
        V = Kennung.Verlauf(an="1", aus="0", Tick=Tick, Skala=False)
        n = len(V)
        k = int(V, 2)
        while k and not k&1:
            k >>= 1
        self.set_kennung(k, n, filename=filename, eeprom=eeprom)
        if n>255 or k >= 2**255:
            self._log("Kennung %s: 0x%08x (%d): zu lang" % (str(Kennung), k, n))
        return V

    def T(self, t, *, Tick=0.5, filename=None, eeprom=False, an='Q', aus="_", **kk):
        if not isinstance(t, Tonnen_Kennung):
            t = Tonnen_Kennung(t, **kk)
        self.Tonne(t, Tick=Tick, filename=filename, eeprom=eeprom)
        if verbose:
            self._log("Kennung %s" % str(t))
            list(map(self._log, t.Verlauf(an, aus, Tick=Tick).split("\n")))
        return t

    def R(self, *, Tick=0.5, eeprom=False, an='Q', aus="_", **kk):
        k = self.get_kennung(eeprom=eeprom)
        d = "" if not Tick else ".%ds" % int(Tick*k[1]+0.5)
        t = Tonnen_Kennung("X(%d,0x%x)%s" % (k[1], k[0], d), **kk)
        self._log("Kennung %s" % str(t))
        list(map(self._log, t.Verlauf(an, aus, Tick=Tick).split("\n")))
        return t

    def E(self, e):
        import traceback
        self._log(repr(e))
        tbs = traceback.extract_tb(e.__traceback__)
        for tb in tbs:
            self._log("%s:%d:%s: %s" % (tb.filename, tb.lineno, tb.name, tb.line))

verbose = True

def usage(short=False):
    sys.stderr.write("""[ipython3] %s [«ipython-options»] [ -- [«options»] [«kennung»] ]
python3 %s [«options»] [«kennung»]

Talk to a blinkenlights board with `kennung.c` firmware.  Kennung is the
German word for the characteristics of the light of a sea mark.

«options»:
-h --help           print this help
-v --verbose        increase verbosity
-q --quiet          zero verbosity
-i --interactive    keep running after processing «kennung»
-n --nop            interactive without processing «kennung»
-F --tty=«tty»      connect via UART «tty»
-o --filename=«fn»  write raw eeprom bytes into file «fn»
   --eeprom         write the pattern into the EEPROM
-T --period=«T»     base tick period to «T», default=0.5
-s --pace=«f»       multiply pattern feature lengths by «f»
""" % (2*(sys.argv[0].split('/')[-1],)))
    if not short:
        sys.stderr.write("""
Without «kennung» arguments you will be dropped to the ipython shell.
You will need to add a line to the profile config:

    echo 'get_config().TerminalIPythonApp.force_interact = True' \\
        >> ~/.ipython/profile_kennung/ipython_config.py

or start with «ipython-options»

    %s -i -- …

When «kennung» is/are present, the program exits after processing
those, unless the options --interactive or --nop are present.

When the option --filename= is present, the last light pattern is
written to that file, that can be written into the ATtiny EEPROM with
avrdude.  --filename=- writes to stdout (binary data).

When --tty= and --eeprom is present, the pattern is written into the
ATtiny EEPROM.

Else, when -tty is present, the pattern is written into the RAM.

Else, or with --interactive or --nop, drop to the ipython shell.

With --nop no processing is done with «kennung» apart from parsing
into a list P[].

On the shell you get access to an instance K=Kennung(), optionally
.connect()ed to the --tty=.

The parsed pattern «kennung» are provided in the list P[].

The method R(Tick=0.5, eeprom=False, Faktor=None) reads a pattern from
RAM or EEPROM.

The method T(t, Tick=0.5, filename=None, eeprom=False, Faktor=None)
takes a «kennung» string or a Tonnen_Kennung() instance and sends it
to RAM, EEPROM or the file.

«kennung» should accept most strings as you find on nautic maps next
to a sea mark with a light.

Kardinal Marks are 'Q', 'Q(3).9s', 'Q(6)+LFl.15s', 'Q(9).12s'.
Lateral Mark 18/K1 at the entrance to the Kiel canal is 'Fl(2+1)R.15s'.

""" % sys.argv[0].split('/')[-1])
    os._exit(0)

P = []

def main(argv=sys.argv[1:]):
    global verbose

    import getopt
    try:
        oo, kk = getopt.getopt(argv,
                               "hvqiF:nk:T:s:o:",
                               ["interactive", "verbose", "quiet", "help", "nop",
                                "eeprom", "tty=", "filename=",
                                "length=", "period=", "pace="])
    except Exception as e:
        sys.stderr.write("\nError: %s\n\n" % repr(e))
        usage(short=True)

    tty = None
    n = None
    interactive = not kk
    eeprom = False
    Tick = 0.5
    Faktor = None
    Filename = None

    for o,v in oo:
        if o in "-h --help":
            usage()
        if o in "--eeprom":
            eeprom = True
        if o in "-i --interactive":
            interactive = True
        if o in "-n --nop":
            interactive = 2
        if o in "-v --verbose":
            verbose += 1
        if o in "-q --quiet":
            verbose = False
        if o in "-F --tty":
            tty = v
        if o in "-o --filename":
            Filename = v
        if o in "-n --length":
            n = int(v,0)
        if o in "-t --period":
            Tick = float(v)
        if o in "-s --pace":
            Faktor = float(v)

    if eeprom:
        kk[:-1] = []

    K = Kennung(tty)
    if eeprom or verbose > 1:
        K.verbose = True

    for k in kk:
        P.append(Tonnen_Kennung(k, Faktor=Faktor))
        
    if kk and Filename:
        K.T(P[-1], Tick=Tick, filename=Filename)

    if tty and interactive < 2:
        if kk and (eeprom or not Filename):
            try:
                if verbose:
                    K.R(Tick=Tick, eeprom=eeprom)
                K.T(P[-1], Tick=Tick, eeprom=eeprom)
            except Exception as e:
                K.E(e)
        if verbose:
            try:
                K.R(Tick=Tick, eeprom=eeprom)
            except Exception as e:
                K.E(e)

    if not interactive:
        os._exit(0)

    return K

if __name__=="__main__":
    K = main()
    T = K.T
    R = K.R
