//
// blink.c
//

// !!! int = int8_t

#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>

#define Bit(x) (1<<(x))
#define SetPORT(x) PORTB |= Bit(x)
#define ClrPORT(x) PORTB &=~ Bit(x)
#define GetPORT(x) (PINB & Bit(x))

#define RES_PORT  0
#define TRIG_PORT 1
#define OUT_PORT  2
#define LED2_PORT 3
#define LED1_PORT 4

#define START_PORT OUT_PORT

// 9.6MHz/8/240 = 5kHz
#define MAX_DC 240
#define DC_EXP 12

static uint32_t led1_dc;

static inline
uint8_t get_dc(uint32_t dc) {
	return dc >> 24;
}
static inline
uint32_t set_dc(uint8_t dc) {
	return ((uint32_t)dc << 24) - 1;
}
static inline
uint32_t fade(uint32_t dc)
{
	uint32_t diff = dc >> (8*((DC_EXP+4)>>3));
	if (DC_EXP & 4)
		diff <<= 8 - (DC_EXP&7);
	else
		diff >>= DC_EXP & 3;
	return dc - diff;
}

ISR(TIM0_COMPB_vect)
{
	SetPORT(LED1_PORT);
}

ISR(TIM0_COMPA_vect)
{
	TIFR0 = Bit(OCF0B);
	ClrPORT(LED1_PORT);
	OCR0B = MAX_DC + 1 - (uint8_t)(led1_dc >> 24);
}

static inline
void pulse_led1(uint8_t dc)
{
	led1_dc = set_dc(dc);
	OCR0B = MAX_DC + 1 - dc;
}

static inline
void init_timer()
{
	DDRB = Bit(LED1_PORT) | Bit(LED2_PORT);
	ClrPORT(LED1_PORT);
	TCCR0A = 0x02; // CTC TOP=OCRA TOV=MAX
	TCCR0B = 0x02; // clk_IO/8
	OCR0A  = MAX_DC;
	OCR0B  = 255;  // off
	TIMSK0 = Bit(OCIE0A) | Bit(OCIE0B);
}

ISR(PCINT0_vect)
{
	SetPORT(LED2_PORT);
	if (!GetPORT(START_PORT))
		pulse_led1(MAX_DC);
	ClrPORT(LED2_PORT);
}

static inline
void init_trig()
{
	PCMSK = Bit(START_PORT);
	GIMSK = Bit(PCIE);
}

int main()
{
	init_timer();
	init_trig();
	pulse_led1(MAX_DC);
	set_sleep_mode(SLEEP_MODE_IDLE);
	uint16_t debug = 0;
	while (1) {
		if (!debug) { 
			// debug = 0xff00L | OCR0B;
			ClrPORT(LED2_PORT);
		}
		else {
			if (debug&1)
				SetPORT(LED2_PORT);
			else
				ClrPORT(LED2_PORT);
			debug >>= 1;
		}
		sei();
		sleep_mode();
		led1_dc = fade(led1_dc);
	}
}
