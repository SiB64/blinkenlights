#! /usr/bin/python3

import sys, serial, struct, time

def calibrate(Word, D):

    debug = 0
    
    C=[0]*7
    C[1] = Word[1] >> 1
    C[2] = ((Word[3] & 0x3f)<<6) | (Word[4] & 0x3f) 
    C[3] = Word[4]>>6
    C[4] = Word[3]>>6
    C[5] = ((Word[1] & 1)<<10) | (Word[2]>>6)
    C[6] = Word[2] & 0x3f

    if debug:
       sys.stdout.write("""
    C1 = %d
    C2 = %d
    C3 = %d
    C4 = %d
    C5 = %d
    C6 = %d
    """ % tuple(C[1:7]))
    
    UT1 = 8*C[5]+20224
    dT = D[2] - UT1
    TEMP = 200 + dT*(C[6]+50)/1024
    
    if debug:
       sys.stdout.write("""
    D2 = %d
    UT1 = %d
    dT  = %d
    TEMP = %.2f °C
    """ % (D[2], UT1, dT, TEMP*0.1))
    
    OFF = C[2]*4 + ((C[4]-512)*dT)/4096
    SENS = C[1] + (C[3]*dT)/1024 + 24576
    X = (SENS * (D[1]-7168))/16384 - OFF
    P = X*10/32 + 2500
    
    if debug:
       sys.stdout.write("""
    D1 = %d
    OFF = %d
    SENS = %d
    X = %d
    P = %.2f mbar
    """ % (D[1], OFF, SENS, X, P*0.1))

    return (TEMP/10, P/10)

port = "/dev/ttyUSB1"

def read_bate(port):
    tty = serial.Serial(port=port, baudrate=2400)
    r = b''
    nx = 0
    while True:
        r += tty.read_all()
        if len(r)<16:
            r += tty.read(16-len(r))
        while r and r[0] != 0x7e or r[1:] and r[1] != 0xba:
            sys.stdout.write(f" {r[0]:02x}")
            r = r[1:]
            nx += 1
            if nx >= 32:
                sys.stdout.write("\n")
                sys.stdout.flush()
                nx = 0
        if len(r)<16:
            continue
        w = struct.unpack("<8H", r[:16])
        r = r[16:]
        T,p = calibrate(w[1:],w[5:])
        t = time.time()
        sys.stdout.write(f"\nPT {t:.1f} {time.strftime('%Y-%m-%dT%H:%M:%SZ', time.gmtime(t))} {T:+5.2f} °C {p:7.2f} mbar ")
        sys.stdout.write(" ".join([f"{x:04x}" for x in w]))
        sys.stdout.flush()
        nx = 0

def recalibrate(f):
    for l in f.readlines():
        ll = l.split()
        if len(ll) != 15:
            continue
        if ll[0] != "PT":
            continue
        w = [int(lll,16) for lll in ll[-8:]]
        t = float(ll[1])
        T,p = calibrate(w[1:],w[5:])
        sys.stdout.write(f"\nPT {t:.1f} {time.strftime('%Y-%m-%dT%H:%M:%SZ', time.gmtime(t))} {T:+5.2f} °C {p:7.2f} mbar ")
        sys.stdout.write(" ".join([f"{x:04x}" for x in w]))

if __name__ == "__main__":
    if len(sys.argv) == 2:
        port = sys.argv[1]
    if port == "recalibrate":
        recalibrate(sys.stdin)
    else:
        read_bate(port)
    sys.stdout.write("\n")
