//
// kennung.c
//

// !!! int = int8_t

#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/wdt.h>

#ifdef SAFE_EEPROM
#  include <avr/eeprom.h>
#else
#  include "eeprom.h"
#  define eeprom_update_byte unsafe_eeprom_write_byte
#  define eeprom_read_byte   unsafe_eeprom_read_byte
#  define eeprom_read_block  unsafe_eeprom_read_block
#endif

#define Bit(x) (1<<(x))
#define SetPORT(x) PORTB |= Bit(x)
#define ClrPORT(x) PORTB &=~ Bit(x)
#define GetPORT(x) (PINB & Bit(x))

#define RES_PORT  0
#define TRIG_PORT 1
#define OUT_PORT  2
#define LED2_PORT 3
#define LED1_PORT 4

#define Rx_PORT RES_PORT
#define Tx_PORT TRIG_PORT

/////////////////////////////////////////////////////////////////////////////
//
// LED1 exponential rise and fade.

// 9.6MHz/8/250 = 4.8kHz → 2400 baud
// Use this parameter on the make commandline to adjust the frequency error.
// E.g., `make kennung.hex -D MAX_DC=251 -W kennung.c`
#ifndef MAX_DC
# define MAX_DC 250
#endif
#define DC_MARGIN 5

typedef uint16_t dc_t;
#define DC_SHIFT (((sizeof (dc_t))-1)*8)
static dc_t led1_dc;
static uint8_t tick;

static inline
dc_t fade(dc_t dc)
{
	dc_t diff = dc >> 8;
	return dc - diff;
}

static inline
dc_t rise(dc_t dc)
{
	// Keep some margin.  When dc becomes too high, the COMPB
	// interrupt may not execute because is is blocked by the
	// COMPA interrupt.
	dc_t diff = (((dc_t)(MAX_DC - DC_MARGIN)<<DC_SHIFT) - dc) >> 8;
	return dc + diff;
}

ISR(TIM0_COMPB_vect)
{
	SetPORT(LED1_PORT);
}

ISR(TIM0_COMPA_vect)
{
	// Clr the COMPB interrupt, in case it came before the COMPA
	// interrupt, but gets executed afterwards due to lower
	// priority.
	TIFR0 = Bit(OCF0B);
	ClrPORT(LED1_PORT);
	// dc==0 → OCR0B = MAX_DC+1, i.e., never reached, off
	OCR0B = MAX_DC + 1 - (uint8_t)(led1_dc >> DC_SHIFT);
	tick = 1;
}

static inline
void init_timer()
{
	ClrPORT(LED1_PORT);
	TCCR0A = 0x02; // CTC TOP=OCRA TOV=MAX
	TCCR0B = 0x02; // clk_IO/8
	OCR0A  = MAX_DC;
	OCR0B  = 255;  // off
	TIMSK0 = Bit(OCIE0A) | Bit(OCIE0B);
}

/////////////////////////////////////////////////////////////////////////////
//
// Send a characteristic light pattern (German: Kennung)
//
// Triggered by the NE555

#define NK 32

struct kennung {
	union {
		uint8_t length;
		uint8_t bits[NK+1];
	};
};

static struct kennung kennung;
static struct kennung EEMEM eeprom;

static uint8_t phase;

ISR(PCINT0_vect)
{
	static uint8_t position, index;

	if (!GetPORT(OUT_PORT))
		return;

	if (++position >= kennung.length) {
		position = 0;
		index = 0;
		phase = kennung.bits[1];
	}
	else if (!(position & 7))
		phase = kennung.bits[++index+1L];
	else
		phase >>= 1;
}

static inline
void init_trig()
{
	PCMSK = Bit(OUT_PORT);
	GIMSK = Bit(PCIE);
}

/////////////////////////////////////////////////////////////////////////////
//
// Receive the characteristic via 2400 baud UART

static uint8_t send_char;
static uint8_t send_bits;

static inline
void send(uint8_t c)
{
	send_char = c;
	send_bits = 20;
}

static inline
void send_bit() {
	if (!send_bits)
		return;
	send_bits--;
	if (!(send_bits & 1))
		return;
	if (send_bits >= 19)
		ClrPORT(Tx_PORT);
	else if (send_bits == 1)
		SetPORT(Tx_PORT);
	else {
		if (send_char & 1)
			SetPORT(Tx_PORT);
		else
			ClrPORT(Tx_PORT);
		send_char >>= 1;
	}
}

static inline
void recv(uint8_t c)
{
	static uint8_t n, nn;

	if ('0' <= c && c <= '9') {
		SetPORT(LED2_PORT);
		n *= 10;
		n += (c & 0xf);
		return;
	}
	ClrPORT(LED2_PORT);

	uint8_t a = c;

	if (c=='w')
		kennung.bits[nn] = n;
	else if (c=='r')
		a =  '0' | 7 & (kennung.bits[nn] >> n);
	else if (c=='v')
		a =  '0' | 7 & (eeprom_read_byte(eeprom.bits+nn) >> n);
	else if (c=='e') 
		eeprom_update_byte(eeprom.bits+nn, n);
	else if (c=='x' && n==55) 
		while (1) ;
	else if (c!=',')
		a = '?';

	send(a);

	nn = n;
	n = 0;
}

static inline
void uart()
{
	static uint8_t c;
	static uint8_t n;
	static uint8_t bb;
	static uint8_t b;

	// get an early copy, then do some other work
	bb = b;
	b = GetPORT(Rx_PORT);
	
	send_bit();

	if (!n) {
		if (!b)
			n = 1;
		return;
	}
	n++;
	if (!(n & 1)) {
		if (b == bb)
			return;
		// we saw a bit only once.
		n++;
	}
	// get a late copy in case the early copy sees the same bit
	// trice.
	b = GetPORT(Rx_PORT);
	if (n == 19) {
		if (b)
			recv(c);
		n = 0;
		return;
	}
	c >>= 1;
	if (b)
		c |= 0x80;
}

int main()
{
	eeprom_read_block(&kennung, &eeprom, sizeof(kennung));

	DDRB = Bit(LED1_PORT) | Bit(LED2_PORT) | Bit(Tx_PORT);

	init_timer();
	init_trig();

	send('x');

	set_sleep_mode(SLEEP_MODE_IDLE);
	while (1) {
		sei();
		sleep_mode();
		wdt_reset();
		if (!tick)
			continue;
		tick = 0;
		if (phase & 1)
			led1_dc = rise(led1_dc);
		else
			led1_dc = fade(led1_dc);
		uart();
	}
}
