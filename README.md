# Blinkenlights

This circiut incorporates two most favorite chips, an LM317 (Björns favorite) and an NE555 (my favorite).  In our lab the soldering irons run at 317°C.  I did not try 555°C.  

![SN1](https://codeberg.org/SiB64/blinkenlights/raw/branch/master/IMG_4564-2.jpg)
![Layout](https://codeberg.org/SiB64/blinkenlights/raw/branch/master/blinkenlights.png)
[![Schematics](https://codeberg.org/SiB64/blinkenlights/raw/branch/master/blinkenlights_sch.png)](https://codeberg.org/SiB64/blinkenlights/raw/branch/master/blinkenlights_sch.pdf)
