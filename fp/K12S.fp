
Element["" "" "S?" "" 30.0000mm 40.0000mm -0.7000mm -0.8000mm 0 100 ""]
(
	Pin[0.0000 0.0000 3.6000mm 0.5000mm 3.7524mm 3.1000mm "5" "5" ""]
	Pad[-2.5000mm 5.0000mm -2.3000mm 5.0000mm 2.2000mm 0.5000mm 2.7000mm "2" "2" "square"]
	Pad[2.3000mm 5.0000mm 2.5000mm 5.0000mm 2.2000mm 0.5000mm 2.7000mm "1" "1" "square,edge2"]
	Pad[2.3000mm -5.0000mm 2.5000mm -5.0000mm 2.2000mm 0.5000mm 2.7000mm "4" "4" "square,edge2"]
	Pad[-2.5000mm -5.0000mm -2.3000mm -5.0000mm 2.2000mm 0.5000mm 2.7000mm "3" "3" "square"]
	ElementLine [-1.0000mm -6.0000mm -1.0000mm -4.0000mm 0.1600mm]
	ElementLine [-1.0000mm -4.0000mm 1.0000mm -4.0000mm 0.1600mm]
	ElementLine [1.0000mm -4.0000mm 1.0000mm -6.0000mm 0.1600mm]
	ElementLine [1.0000mm -6.0000mm -1.0000mm -6.0000mm 0.1600mm]
	ElementLine [-1.0000mm 6.0000mm -1.0000mm 4.0000mm 0.1600mm]
	ElementLine [-1.0000mm 4.0000mm 1.0000mm 4.0000mm 0.1600mm]
	ElementLine [1.0000mm 4.0000mm 1.0000mm 6.0000mm 0.1600mm]
	ElementLine [1.0000mm 6.0000mm -1.0000mm 6.0000mm 0.1600mm]
	ElementArc [0.0000 0.0000 5.8000mm 5.8000mm 270.000000 90.000000 0.1600mm]
	ElementArc [0.0000 0.0000 5.8000mm 5.8000mm 180.000000 90.000000 0.1600mm]
	ElementArc [0.0000 0.0000 5.8000mm 5.8000mm 0.000000 90.000000 0.1600mm]
	ElementArc [0.0000 0.0000 5.8000mm 5.8000mm 90.000000 90.000000 0.1600mm]

	)
