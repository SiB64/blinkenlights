
Element["" "" "R?" "" 900.00mil 450.00mil -175.00mil -100.00mil 0 100 ""]
(
	Pin[100.00mil 0.0000 60.00mil 30.00mil 66.00mil 28.00mil "" "1" "edge2"]
	Pin[0.0000 0.0000 60.00mil 30.00mil 66.00mil 28.00mil "" "2" "edge2"]
	Pin[-100.00mil 0.0000 60.00mil 30.00mil 66.00mil 28.00mil "" "3" "edge2"]
	ElementLine [-200.00mil -100.00mil 200.00mil -100.00mil 0.1500mm]
	ElementLine [200.00mil -100.00mil 200.00mil 100.00mil 0.1500mm]
	ElementLine [-200.00mil 100.00mil -200.00mil -100.00mil 0.1500mm]
	ElementLine [-175.00mil 100.00mil -175.00mil 75.00mil 0.1500mm]
	ElementLine [-175.00mil 75.00mil 175.00mil 75.00mil 0.1500mm]
	ElementLine [175.00mil 75.00mil 175.00mil 100.00mil 0.1500mm]
	ElementLine [-200.00mil 100.00mil -175.00mil 100.00mil 0.1500mm]
	ElementLine [175.00mil 100.00mil 200.00mil 100.00mil 0.1500mm]
	ElementLine [95.00mil -5.00mil 160.00mil -75.00mil 0.1500mm]
	ElementLine [175.00mil -65.00mil 110.00mil 5.00mil 0.1500mm]
	ElementArc [135.00mil -35.00mil 50.00mil 50.00mil 180.000000 90.000000 0.1500mm]
	ElementArc [135.00mil -35.00mil 50.00mil 50.00mil 90.000000 90.000000 0.1500mm]
	ElementArc [135.00mil -35.00mil 50.00mil 50.00mil 0.000000 90.000000 0.1500mm]
	ElementArc [135.00mil -35.00mil 50.00mil 50.00mil 270.000000 90.000000 0.1500mm]

	)
