#!/usr/bin/python2
# coding=utf-8

# $Id: gvp2make.py 8411 2022-02-28 11:50:10Z stephan $
# This script is free software (c) 2010 Stephan I. Böttcher
# Distributed under GNU GPL Version 2 or later.

"""
Julian <thepurlieu@gmail.com> writes:

> Stephan,
>     Yes, you can do it, however you can't use the project file for the
> process.  Here's how:
>
> gerbv --export=png --dpi=600 --foreground=#ff0000ff
> --foreground=#00ff0088 file1.gbx file2.gbx .......and so on
>
> Hope this helps.  Cheers--
"""


usage="""
Prints a gerbv commandline to export a project to png,
with adjusted layer opacities.
Options:
        -h		print this help
        -g gerbv 	path of the gerbv executable
        -x png		export format
        -o layout.png~  output file
        -D 600		dpi resolution
        -w <option>	gerbv option
        -A layer=alpha	alpha between 0 and 1.0
        -X		execute the command
        -M		print Makefile to stdout
"""

options=[
    "gerbv",
    "--export=png",
    "--output=layout.png~",
    "--dpi=600",
    ]

"""
The layer names are the second dot-separated part of the filename.
Digits are stripped from the layer name and tried again.
"""

opacities = {
    "DEFAULT": 0.7,
    "frontsilk": 1.0,
    "topsilk": 1.0,
    "bottomsilk": 1.0,
    "backsilk": 1.0,
    "outline": 1.0,
    "plated-drill": 1.0,
    "frontpaste": 0.5,
    "backpaste": 0.5,
    "front": 0.3,
    "component": 0.3,
    "back": 0.3,
    "backside": 0.3,
    "group": 0.2,
    "signal": 0.2,
    "ground": 0.1,
    "grounds": 0.1,
    "power": 0.1,
    "nocoating": 0.5,
    }
    
execcmd=False
makefile=False

import sys, os
from getopt import gnu_getopt as getopt
oo,ifile = getopt(sys.argv[1:], "hg:x:o:w:D:XMA:")
for o,v in oo:
    if o=="-h":
        print "Synopsis:", sys.argv[0], "<options> gerbv-project-file", usage
        sys.exit()
    if o=="-g":
        options[0]=v
    if o=="-x":
        options[1]="--export=%s" % v
    if o=="-o":
        options[2]="--output=%s" % v
    if o=="-D":
        options[3]="--dpi=%s" % v
    if o=="-w":
        options.append(v)
    if o=="-X":
        execcmd=True
    if o=="-M":
        makefile=True
    if o=="-A":
        vv=v.split("=")
        opacities[vv[0]]=float(vv[1])

def parselayer(l):
    r = {}
    for ll in l.strip().split("(cons"):
        lll = ll.strip().split(" ",1)
        r[lll[0].strip("'#()")] = lll[1].strip("#()")
    return r

layers = {}
from fileinput import input

LL=""
for l in input(ifile):
    LL+=l.strip()
    nopen=0
    nclose=0
    for L in LL:
        if L=='(':
            nopen += 1
        if L==')':
            nclose += 1
    if nopen==nclose:
        ll = parselayer(LL)
        if "define-layer!" in ll and "visible" in ll and ll["visible"]=="t":
            layers[int(ll["define-layer!"])] = ll
        LL=""

files=[]
lnumbers = layers.keys()
lnumbers.sort()
for n in lnumbers:
    ll = layers[n]
    fn = ll["filename"].strip('"')
    files.append(fn)
    try:
        ltype = fn.split(".")[1]
        try:
            opacity = opacities[ltype]
        except KeyError:
            opacity = opacities[ltype.strip("0123456789")]
    except:
        opacity = opacities["DEFAULT"]
    color = [int(c)/65536. for c in ll["color"].split()]
    if len(color)<4:
        color.append(opacity)
    options.append("--foreground=#%02x%02x%02x%02x" % tuple(
            [int(c*255) for c in color]))

if makefile:
    target = options[2].split("=")[1]
    print "GERBV="+options[0]
    print "FORMAT="+options[1].split("=")[1]
    print "DPI="+options[3].split("=")[1]
    print target+": \\\n\t  "+" \\\n\t  ".join(files)
    print "\t$(GERBV) --output=$@ --export=$(FORMAT) --dpi=$(DPI) \\"
    print "\t  "+" \\\n\t  ".join(options[4:])+" \\\n\t $<"
else:
    cmd = " ".join(options+files)
    print cmd
    if execcmd:
        os.system(cmd)
